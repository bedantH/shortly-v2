import { extendTheme } from "@chakra-ui/react";

const customTheme = extendTheme({
    colors: {
        primary: "#2acfcf",
        darkViolet: "#3b3054",
        red: "#f46262",
        gray: "#bfbfbf",
        grayishViolet: "#9e9aa7",
        veryDarkBlue: "#35323e",
        veryDarkViolet: "#232127",
        primaryBg: "#fff",
        hover: "#56E7E7",
        secondaryBg: "#f0f1f6",
    }
});

export { customTheme }