import Axios from "axios";

const http = Axios.create({
    baseURL: process.env.REACT_URLSHORTNER_URI,
    headers: {
        "Content-Type": "application/json"
    },
});

export default http;