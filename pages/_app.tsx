import '../styles/globals.css'
import type { AppProps } from 'next/app';
import ResourceProvider from '../components/providers/ResourceProvider';
import { config } from '../resources/resources.provider';
import { ChakraProvider } from '@chakra-ui/react'
import { customTheme } from '../lib/theme';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ResourceProvider value={config}>
      <ChakraProvider resetCSS theme={customTheme}>
        <Component {...pageProps} />
      </ChakraProvider>
    </ResourceProvider>
  )
}

export default MyApp
