import { HeaderComponent } from '../components/HeaderComponent';
import type { NextPage } from 'next'
import Page from '../components/layouts/Page'
import HeroComponent from '../components/HeroComponent';
import URLShortnerLayout from '../components/layouts/URLShortnerLayout';
import { Box } from '@chakra-ui/react';
import StatisticsLayout from '../components/layouts/StatisticsLayout';
import BottomBanner from '../components/layouts/BottomBanner';
import Footer from '../components/layouts/Footer';


const Home: NextPage = () => {
  return (
    <Page title="Main Page">
      <HeaderComponent />
      <HeroComponent />
      <Box as="section" bgColor="secondaryBg" pt="4.5rem" pb="4.5rem">
        <URLShortnerLayout />
        <StatisticsLayout />
      </Box>
      <BottomBanner />
      <Footer />
    </Page>
  )
}

export default Home
