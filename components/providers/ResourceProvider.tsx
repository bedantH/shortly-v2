import React, { createContext, useContext } from 'react';
import { config } from '../../resources/resources.provider';

export const ResourceContext = createContext<any>({});

const ResourceProvider = (props: any) => {
    return (
        <ResourceContext.Provider value={config}>
            {props.children}
        </ResourceContext.Provider>
    )
}

export default ResourceProvider;

export const useResource = () => {
    return useContext(ResourceContext);
}