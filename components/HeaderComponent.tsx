import React, { useEffect } from "react";
import { Box } from '@chakra-ui/react'
import { Header, Image, Flex, Link } from './library';
import { useResource } from "./providers/ResourceProvider";

export function HeaderComponent({ }) {
  const resource = useResource();

  return (
    <Header>
      <Flex flexDirection="row" px={{ xl: 40, sm: 10 }} py={{ xl: 10 }} alignItems="center">
        <Image src="./logo.svg" alt="Logo" w={100} />
        <Box as='nav' marginLeft="1rem">
          <Flex flexDirection="row">
            <Link
              fontSize="0.9rem"
              marginLeft="1.6rem"
              color="grayishViolet"
              fontWeight="700"
              _hover={{ color: "veryDarkBlue" }}
              href="#"
              transition="0.5s">{resource.features}</Link>
            <Link
              fontSize="0.9rem"
              marginLeft="1.6rem"
              color="grayishViolet"
              fontWeight="700"
              _hover={{ color: "veryDarkBlue" }}
              href="#"
              transition="0.5s">{resource.pricing}</Link>
            <Link
              fontSize="0.9rem"
              marginLeft="1.6rem"
              color="grayishViolet"
              fontWeight="700"
              _hover={{ color: "veryDarkBlue" }}
              href="#"
              transition="0.5s">{resource.resources}</Link>
          </Flex>
        </Box>
      </Flex >
    </Header >
  );
}
