import { Container, Heading, Image } from '@chakra-ui/react'
import React from 'react'

const HeroComponent = () => {
    return (
        <Container maxW="100%">
            <Heading maxW="65vw" textAlign="center" fontFamily="Poppins" fontSize={{ xl: "6rem", sm: "4rem" }} as="h1" fontWeight="700" margin="2rem auto 1rem auto">MORE THAN JUST SHORTER LINKS</Heading>
            <Image w="fit-content" margin="0 auto" src="./people%20working.svg" alt='heroImage' />
        </Container>
    )
}

export default HeroComponent
