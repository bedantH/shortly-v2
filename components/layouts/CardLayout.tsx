import { Box, Container, Flex, Image, List, ListItem, Heading } from '@chakra-ui/react'
import React from 'react'
import { Typography } from '../library'

const Card = () => {
    return (
        <Box m={{ xl: '0rem 2rem', sm: '2rem 0rem' }} mx="1.5rem" bgColor='primaryBg' width="fit-content" p="2rem 2rem" pos="relative" maxW="22rem" borderRadius="5px">
            <Box
                bgColor="veryDarkBlue"
                p="1rem 1rem"
                borderRadius="50%"
                pos="absolute"
                top="-20%"
                transform="transalteY(100%)">
                <Image src="/icon-brand-recognition.svg" alt="icon" />
            </Box>
            <Heading fontFamily="Poppins" as="h4" fontSize="1.3rem" mb="1rem" mt="2.5rem" w="fit-content">
                Brand Recognition
            </Heading>
            <Typography fontFamily="Poppins" as="p" fontSize="0.8rem" color="grayishViolet" lineHeight="1.3rem">
                Boost your brand recongnition with each click. Generic links dont mean a thing. Branded links help instil confidence in your content.
            </Typography>
        </Box>
    )
}

const CardLayout = () => {
    return (
        <Box width="fit-content" margin="0rem auto" mt="7rem">
            <Flex flexDirection={{ xl: 'row', lg: 'row', sm: 'column' }} >
                <Card />
                <Card />
                <Card />
            </Flex>
        </Box>
    )
}

export default CardLayout
