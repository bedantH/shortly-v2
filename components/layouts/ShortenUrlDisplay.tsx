import { VStack, Container, Flex, Button } from '@chakra-ui/react'
import React, { useState } from 'react'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { Link } from '../library';

const Card = (props: any) => {
    const [copied, setCopied] = useState<Boolean>(false);

    return (
        <Container as="div" maxW="80vw" bgColor="#fff" p="1rem 2rem" borderRadius="5px" my="0.7rem">
            <Flex justifyContent="space-between" w="100%" alignItems="center">
                <Flex justifyContent="space-between" w="-webkit-fill-available" mr="2rem">
                    <Link fontWeight="500" color="veryDarkBlue">
                        {props.original_link}
                    </Link>
                    <Link fontWeight="500" color="primary" href="shrtco.de/JtMZV7">
                        {props.short_link}
                    </Link>
                </Flex>
                <CopyToClipboard text={props.short_link} onCopy={() => setCopied(true)}>
                    <Button fontSize="0.9rem" color="primaryBg" bgColor={copied ? "veryDarkBlue" : "primary"} p="1rem 2rem">{copied ? "Copied" : "Copy"}</Button>
                </CopyToClipboard>
            </Flex>
        </Container >
    )
}

const ShortenUrlDisplay = (props: any) => {
    return (
        <VStack mt="1.3rem">
            {
                props.urlHistory.map((item: any, index: any) => {
                    return (
                        <Card key={index} {...item} />
                    )
                })
            }
        </VStack>
    )
}

export default ShortenUrlDisplay
