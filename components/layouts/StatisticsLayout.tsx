import { Box, Container, Heading } from '@chakra-ui/react'
import React from 'react'
import { Typography } from '../library'
import CardLayout from './CardLayout'

const StatisticsLayout = () => {
    return (
        <Box>
            <Container textAlign="center" mt="5rem">
                <Heading color="veryDarkBlue" as="h2" fontFamily="Poppins">Advanced Statistics</Heading>
                <Typography
                    fontSize="1.1rem"
                    color="grayishViolet"
                    maxW="90%"
                    margin="0rem auto"
                    mt="1rem"
                    fontWeight="500"
                    lineHeight="1.6">
                    Track how your links are performing across the web with our advanced statistics dashboard.
                </Typography>
            </Container>
            <CardLayout />
        </Box>
    )
}

export default StatisticsLayout
