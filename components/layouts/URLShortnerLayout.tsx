import { Button, Container, Flex, Input, Box as form } from '@chakra-ui/react'
import { url } from 'inspector'
import React, { useState } from 'react'
import ShortenUrlDisplay from './ShortenUrlDisplay'
import { useFormik, FormikProps } from 'formik';
import useAxios from '../../hooks/useAxios';
import axios from 'axios';

interface myFormValues {
    url: string
};

const URLShortnerLayout = () => {

    const [urlHistory, setUrlHistory] = useState<any>([])
    const [result, setResult] = useState<any>({})

    const formik: FormikProps<myFormValues> = useFormik<myFormValues>({
        initialValues: {
            url: ''
        },
        onSubmit: async (values: any) => {
            try {
                await axios.get('https://api.shrtco.de/v2/shorten', {
                    params: {
                        url: values.url
                    }
                }).then(res => {
                    console.log(res.data.result)
                    setUrlHistory((prev: any) => {
                        return [...prev, res.data.result]
                    })
                })
            } catch (err) {
                console.log(err)
            }
        }
    });

    return (
        <React.Fragment>
            <Container maxW="80vw" bgImg="url('/Meteor.png')" p="3rem 6rem" pos="relative" mt="-8.5rem">
                <form onSubmit={formik.handleSubmit}>
                    <Flex>
                        <Input
                            id="url"
                            name="url"
                            border="none"
                            marginRight="1.5rem"
                            fontWeight="normal"
                            placeholder='Shorten a link here...'
                            bg="primaryBg"
                            py="1.6rem"
                            onChange={formik.handleChange}
                            _placeholder={{
                                color: "red"
                            }}
                        />
                        <Button
                            type="submit"
                            bgColor="primary"
                            py="1.6rem"
                            px="2.5rem"
                            color="primaryBg"
                            border="none"
                            _hover={
                                {
                                    bgColor: "hover"
                                }
                            }>
                            Shorten It!
                        </Button>
                    </Flex>
                </form>
            </Container>
            <Container maxW="80vw">
                <ShortenUrlDisplay urlHistory={urlHistory} />
            </Container>
        </React.Fragment>
    )
}

export default URLShortnerLayout
