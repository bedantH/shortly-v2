import Head from 'next/head'
import React from 'react'

const Page = (props: any) => {
    return (
        <React.Fragment>
            <Head>
                <title>{props.title}</title>
            </Head>
            <main>
                {props.children}
            </main>
        </React.Fragment>
    )
}

export default Page