import { Box, Grid, GridItem, Flex, Image, List, ListItem, Heading } from '@chakra-ui/react'
import React, { useState } from 'react'
import { Link } from '../library'
import { FaceBookIcon, PinterestIcon, TwitterIcon, InstagramIcon } from '../library/Icons'

type Props = {
    component: any
}

const IconButon = (props: Props) => {
    const [isHovered, setHovered] = useState(false);

    return (
        <Box transition="0.5s" cursor="pointer" onMouseEnter={() => setHovered(true)} onMouseLeave={() => setHovered(false)} mx="1.5rem" ml="0rem">
            <props.component color={isHovered ? "#2acfcf" : "#fff"} />
        </Box>
    )
}

const Footer = () => {

    return (
        <Box as="footer" bgColor="veryDarkViolet" w="100%" h="auto" pb="5rem">
            <Grid templateColumns="30rem 50rem">
                <GridItem mt="7rem" ml="9rem" >
                    <Box as="div" width="fit-content">
                        <Box mb="2rem">
                            <Image w="100%" src="./logo_white.svg" alt="logo-white" />
                        </Box>
                        <Flex mt="1rem" justifyContent="flex-start" alignItems="center">
                            <IconButon component={FaceBookIcon} />
                            <IconButon component={TwitterIcon} />
                            <IconButon component={PinterestIcon} />
                            <IconButon component={InstagramIcon} />
                        </Flex>
                    </Box>
                </GridItem>
                <GridItem>
                    <List m="7rem 4rem 0rem auto" w="68%">
                        <Flex justifyContent="space-between">
                            <ListItem my={{ sm: '1rem' }} >
                                <List>
                                    <Heading fontSize="1.1rem" fontFamily="Poppins" color="primaryBg" as="h6" mb="1.4rem">Features</Heading>
                                    <ListItem fontSize="1.1rem" color="gray" my="0.7rem" fontWeight="500" _hover={{
                                        color: "primary"
                                    }}>
                                        <Link href="#">Shortening Link</Link>
                                    </ListItem>
                                    <ListItem fontSize="1.1rem" color="gray" my="0.7rem" fontWeight="500" _hover={{
                                        color: "primary"
                                    }}>
                                        <Link href="#">Branded Links</Link>
                                    </ListItem>
                                    <ListItem fontSize="1.1rem" color="gray" my="0.7rem" fontWeight="500" _hover={{
                                        color: "primary"
                                    }}>
                                        <Link href="#">Analytics</Link>
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem my={{ sm: '1rem' }}>
                                <List>
                                    <Heading fontSize="1.1rem" fontFamily="Poppins" color="primaryBg" as="h6" mb="1.4rem">Resources</Heading>
                                    <ListItem fontSize="1.1rem" color="gray" my="0.7rem" fontWeight="500" _hover={{
                                        color: "primary"
                                    }}>
                                        <Link href="#">Blog</Link>
                                    </ListItem>
                                    <ListItem fontSize="1.1rem" color="gray" my="0.7rem" fontWeight="500" _hover={{
                                        color: "primary"
                                    }}>
                                        <Link href="#">
                                            Developers
                                        </Link>
                                    </ListItem>
                                    <ListItem fontSize="1.1rem" color="gray" my="0.7rem" fontWeight="500" _hover={{
                                        color: "primary"
                                    }}>
                                        <Link href="#">
                                            Support
                                        </Link>
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem my={{ sm: '1rem' }} >
                                <List my={{ sm: '2rem 0rem' }}>
                                    <Heading fontSize="1.1rem" fontFamily="Poppins" color="primaryBg" as="h6" mb="1.4rem">Company</Heading>
                                    <ListItem fontSize="1.1rem" color="gray" my="0.7rem" fontWeight="500" _hover={{
                                        color: "primary"
                                    }}>
                                        <Link href="#">About</Link>
                                    </ListItem>
                                    <ListItem fontSize="1.1rem" color="gray" my="0.7rem" fontWeight="500" _hover={{
                                        color: "primary"
                                    }}>
                                        <Link href="#">Our Team</Link>
                                    </ListItem>
                                    <ListItem fontSize="1.1rem" color="gray" my="0.7rem" fontWeight="500" _hover={{
                                        color: "primary"
                                    }}>
                                        <Link href="#">Careers</Link>
                                    </ListItem>
                                    <ListItem fontSize="1.1rem" color="gray" my="0.7rem" fontWeight="500" _hover={{
                                        color: "primary"
                                    }}>
                                        <Link href="#">Contact</Link>
                                    </ListItem>
                                </List>
                            </ListItem>
                        </Flex>
                    </List>
                </GridItem>
            </Grid>
        </Box>
    )
}

export default Footer