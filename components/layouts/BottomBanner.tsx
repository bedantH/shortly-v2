import { Box, Button, Container, Flex, Heading } from '@chakra-ui/react'
import React from 'react'

const BottomBanner = () => {
    return (
        <Box bgImg="./Meteor.png" p="3rem 2rem">
            <Container>
                <Flex flexDirection="column">
                    <Heading fontFamily="Poppins" as="h5" color="primaryBg" fontSize="2rem" textAlign="center" mb="1.2rem">
                        Boost your links today
                    </Heading>
                    <Button
                        color="primaryBg"
                        bgColor="primary"
                        w="fit-content"
                        p="1.5rem 2rem"
                        borderRadius="25px"
                        m="0rem auto"
                        _hover={{
                            bgColor: "hover"
                        }}>
                        Get Started
                    </Button>
                </Flex>
            </Container>
        </Box>
    )
}

export default BottomBanner
