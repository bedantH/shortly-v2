import React from 'react';
import { Box } from '@chakra-ui/react';

const Flex = (props: any) => {
    return (
        <Box display="flex" {...props}>
            {props.children}
        </Box>
    );
};

const Header = (props: any) => {
    return (
        <Box as="header" {...props}>
            {props.children}
        </Box>
    )
}

const Heading = (props: any) => {
    return (
        <Box as={props.as || "h1"} {...props}>
            {props.children}
        </Box>
    );
};

const Image = (props: any) => {
    return (
        <Box as="img" {...props}>
            {props.children}
        </Box>
    );
};

const Link = (props: any) => {
    return (
        <Box as="a" {...props}>
            {props.children}
        </Box>
    );
};

const Typography = (props: any) => {
    return (
        <Box as={props.as || "p"} {...props}>
            {props.children}
        </Box>
    );
};

export { Flex, Heading, Header, Image, Typography, Link }